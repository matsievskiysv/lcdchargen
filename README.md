# Description

![UI of the program](ui.png)

Custom character generator for [HD44780U](https://www.sparkfun.com/datasheets/LCD/HD44780.pdf) LCD
screens.

# Installation

```bash
> pip install lcdchargen
```
